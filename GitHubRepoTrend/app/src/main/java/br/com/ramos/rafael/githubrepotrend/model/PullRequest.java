package br.com.ramos.rafael.githubrepotrend.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Rafael Felipe on 17/05/2017.
 * <p>
 * * The Parceable is for save the PullRequest list on Bundle, for restore again when is necessery, and send from Intent
 */

public class PullRequest implements Parcelable {

    private String html_url;
    private String state;
    private Owner user;
    private String title;
    private String body;
    private String created_at;
    private String updated_at;

    public PullRequest(Parcel in) {
        html_url = in.readString();
        state = in.readString();
        user = (Owner) in.readParcelable(Owner.class.getClassLoader());
        title = in.readString();
        body = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    public PullRequest() {
    }

    public PullRequest(String html_url, Owner user, String title, String created_at, String updated_at, String body, String state) {
        this.html_url = html_url;
        this.user = user;
        this.title = title;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.body = body;
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public Owner getUser() {
        return user;
    }

    public void setUser(Owner user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        return "PullRequest{" +
                "html_url='" + html_url + '\'' +
                ", state='" + state + '\'' +
                ", user=" + user +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel in, int flags) {
        in.writeString(html_url);
        in.writeString(state);
        in.writeParcelable(user, flags);
        in.writeString(title);
        in.writeString(body);
        in.writeString(created_at);
        in.writeString(updated_at);
    }

    public static final Parcelable.Creator<PullRequest> CREATOR = new Creator<PullRequest>() {
        @Override
        public PullRequest createFromParcel(Parcel in) {
            return new PullRequest(in);
        }

        @Override
        public PullRequest[] newArray(int size) {
            return new PullRequest[size];
        }
    };
}
