package br.com.ramos.rafael.githubrepotrend.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rafael Felipe on 17/05/2017.
 * <p>
 * This class represent the first JSON Object from response.
 * A repository has a item list
 * <p>
 * {
 * "total_count": 3168790,
 * "incomplete_results": false,
 * "items": [ ]
 * {
 */

public class RepositoriesResults {

    private ArrayList<Repository> items; //all repositories

    public RepositoriesResults(ArrayList<Repository> items) {
        this.items = items;
    }

    public ArrayList<Repository> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return "Results{" +
                "items=" + items +
                '}';
    }
}
