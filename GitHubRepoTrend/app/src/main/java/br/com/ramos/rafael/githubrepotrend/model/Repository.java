package br.com.ramos.rafael.githubrepotrend.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Rafael Felipe on 16/05/2017.
 *
 * The Parceable is for save the repository list on Bundle, for restore again when is necessery, and send from Intent
 */

public class Repository implements Parcelable {

    private String name;
    private String full_name;
    private String description;
    private Owner owner;
    private String stargazers_count;
    private String forks_count;

    private Repository(Parcel in) {
        name = in.readString();
        full_name = in.readString();
        description = in.readString();
        owner = (Owner) in.readParcelable(Owner.class.getClassLoader());
        stargazers_count = in.readString();
        forks_count = in.readString();
    }

    public Repository() {
    }

    public Repository(String name, String full_name, String description, Owner owner, String stargazers_count, String forks_count) {
        this.name = name;
        this.full_name = full_name;
        this.description = description;
        this.owner = owner;
        this.stargazers_count = stargazers_count;
        this.forks_count = forks_count;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(String stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    public String getForks_count() {
        return forks_count;
    }

    public void setForks_count(String forks_count) {
        this.forks_count = forks_count;
    }

    @Override
    public String toString() {
        return "Repository{" +
                "name='" + name + '\'' +
                ", full_name='" + full_name + '\'' +
                ", description='" + description + '\'' +
                ", owner=" + owner +
                ", stargazers_count='" + stargazers_count + '\'' +
                ", forks_count='" + forks_count + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(full_name);
        dest.writeString(description);
        dest.writeParcelable(owner, flags);
        dest.writeString(stargazers_count);
        dest.writeString(forks_count);
    }

    public static final Parcelable.Creator<Repository> CREATOR = new Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };
}
