package br.com.ramos.rafael.githubrepotrend.util_;

import android.app.Application;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by Rafael Felipe on 18/05/2017.

 */

public class JodaTimeAplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        JodaTimeAndroid.init(this);
    }
}
