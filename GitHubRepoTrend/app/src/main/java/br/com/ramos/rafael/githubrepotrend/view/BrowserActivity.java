package br.com.ramos.rafael.githubrepotrend.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import br.com.ramos.rafael.githubrepotrend.R;
import br.com.ramos.rafael.githubrepotrend.model.PullRequest;
import br.com.ramos.rafael.githubrepotrend.util_.GitConstants;
import br.com.ramos.rafael.githubrepotrend.util_.ToobarConfig;

public class BrowserActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private WebView webView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        loadViewsXml();

        Intent intent = getIntent();
        if (intent != null) {
            PullRequest pullRequest   = (PullRequest) intent.getParcelableExtra(GitConstants.PULL_REQUEST); //PullRequest object from Itent
            if (pullRequest != null) {
                getSupportActionBar().setTitle(pullRequest.getTitle()); //change toolbar title

                webView.loadUrl(pullRequest.getHtml_url()); //send the PullRequest URL to webview
            }
        }
    }

    /**
     * Carrega os ViewXml e configura as variaveis
     */
    private void loadViewsXml() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setMax(100);
        toolbar = (Toolbar) findViewById(R.id.toolbarBrowser);
        ToobarConfig.setToolbar(toolbar, this, this);
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_close);
        webView = (WebView) findViewById(R.id.webviewLinkExterno);
        configuraWebView(webView);
    }

    /**
     * WebView cofig
     * @param webView webview to config
     */
    private void configuraWebView(final WebView webView) {
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                progressBar.setProgress(newProgress); //Load the Progressbar with the page progress
            }
        });
    }
}
