package br.com.ramos.rafael.githubrepotrend.util_;

/**
 * Created by Rafael Felipe on 17/05/2017.
 */

public class GitConstants {


    public static final String URL_BASE = "https://api.github.com/"; //BaseURL

    public static final String REPOSITORY = "repository"; //intent key
    public static final String PULL_REQUEST = "pull_request"; //intent key

    public static final String CLOSED = "closed"; //pullrequest state
    public static final String OPEN = "open";//pullrequest state
}
