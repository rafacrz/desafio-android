package br.com.ramos.rafael.githubrepotrend.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;

import java.util.ArrayList;

import br.com.ramos.rafael.githubrepotrend.R;
import br.com.ramos.rafael.githubrepotrend.adapter.PullRequestAdapter;
import br.com.ramos.rafael.githubrepotrend.adapter.RecyclerViewOnClickListenerHack;
import br.com.ramos.rafael.githubrepotrend.model.PullRequest;
import br.com.ramos.rafael.githubrepotrend.model.Repository;
import br.com.ramos.rafael.githubrepotrend.request.CustomRequestJsonArray;
import br.com.ramos.rafael.githubrepotrend.util_.GitConstants;
import br.com.ramos.rafael.githubrepotrend.util_.ToobarConfig;

public class PullRequestsActivity extends AppCompatActivity {

    private static final String PULL_REQUEST_LIST_SAVE_STATE = "pr_list_save_state"; //key bundle
    private static final String REPOSITORY_OBJECT_STATE = "pr_object_state"; //key bundle
    private static final String PULL_REQUEST_OPEN_STATE = "pr_open_state"; //key bundle
    private static final String PULL_REQUEST_CLOSED_STATE = "pr_closeed_state"; //key bundle

    int mOpened = 0, mClosed = 0; //PR open and closeds

    private TextView tvOpened, tvClosed;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private PullRequestAdapter repositoryAdapter;

    private RequestQueue requestQueue;
    private LinearLayoutManager layoutManager;
    private AVLoadingIndicatorView loadingIndicator;

    private Repository mRepository;
    private ArrayList<PullRequest> mPullRequestArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);

        requestQueue = Volley.newRequestQueue(this); //init volley

        initWidgetsViews();

        mPullRequestArrayList = new ArrayList<PullRequest>();
        repositoryAdapter = new PullRequestAdapter(this, mPullRequestArrayList);

        Intent intent = getIntent(); //get intent
        if (intent != null) {
            Repository repository = (Repository) intent.getParcelableExtra(GitConstants.REPOSITORY); //Repository model received from RepositoryActiviy Intent
            if (repository != null) {

                mRepository = repository;

                if (savedInstanceState != null) {
                    mRepository = savedInstanceState.getParcelable(REPOSITORY_OBJECT_STATE);
                }

                String creator = mRepository.getOwner().getLogin(); //name author
                String repositoryName = mRepository.getName(); //repository name
                getSupportActionBar().setTitle(repositoryName); //change de Toolbar title

                //First time the app is opened. The bundle is null
                if (savedInstanceState == null) {
                    //send url to request api - All PullRequests
                    getRepositoriesFromGitHub(GitConstants.URL_BASE + "repos/" + creator + "/" + repositoryName + "/pulls?state=all");
                } else {
                    //When the bundle is changed, orientation screen
                    //show to open and mClosed PR
                    tvOpened.setText(savedInstanceState.getInt(PULL_REQUEST_OPEN_STATE) + " open ");
                    tvClosed.setText(savedInstanceState.getInt(PULL_REQUEST_CLOSED_STATE) + " closed");

                    mPullRequestArrayList = savedInstanceState.getParcelableArrayList(PULL_REQUEST_LIST_SAVE_STATE);
                    repositoryAdapter = new PullRequestAdapter(this, mPullRequestArrayList);
                    recyclerView.setAdapter(repositoryAdapter);
                    sendPullRequestModelObjectToCustonBroser(mPullRequestArrayList);
                }
            }
        }

    }

    /**
     * Widgets xml layout referece with java
     */
    private void initWidgetsViews() {
        tvOpened = (TextView) findViewById(R.id.tvPrOpen);
        tvClosed = (TextView) findViewById(R.id.tvRpClosed);

        loadingIndicator = (AVLoadingIndicatorView) findViewById(R.id.loadingPR);
        toolbar = (Toolbar) findViewById(R.id.toolbarPull);
        ToobarConfig.setToolbar(toolbar, this, this);
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_material);

        recyclerView = (RecyclerView) findViewById(R.id.recylerPullRequestList);
        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(PULL_REQUEST_OPEN_STATE, mOpened);
        outState.putInt(PULL_REQUEST_CLOSED_STATE, mClosed);
        outState.putParcelable(REPOSITORY_OBJECT_STATE, mRepository);
        outState.putParcelableArrayList(PULL_REQUEST_LIST_SAVE_STATE, mPullRequestArrayList); //save the current repository list on bundle
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mOpened = savedInstanceState.getInt(PULL_REQUEST_OPEN_STATE);
        mClosed = savedInstanceState.getInt(PULL_REQUEST_CLOSED_STATE);
        mRepository = savedInstanceState.getParcelable(REPOSITORY_OBJECT_STATE);
        mPullRequestArrayList = savedInstanceState.getParcelableArrayList(PULL_REQUEST_LIST_SAVE_STATE); //restore the last repository list from bundle
    }

    /**
     * This method connect to GitHub API using Volley
     */
    private void getRepositoriesFromGitHub(String url) {

        //CustonRequestJsonOArray request response
        CustomRequestJsonArray requestJsonObject = new CustomRequestJsonArray(url, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {

                if (response != null) {
                    Gson gson = new GsonBuilder().create();
                    final PullRequest[] pullRequests = gson.fromJson(response.toString(), PullRequest[].class);  //Create a parser of Json array Response

                    for (PullRequest pr : pullRequests) {
                        mPullRequestArrayList.add(pr);

                        repositoryAdapter.addListItem(pr, pullRequests.length - 1); //add item to the adapter

                        //count itens open or mClosed
                        if (pr.getState().equals(GitConstants.OPEN)) {
                            mOpened++;
                        } else {
                            mClosed++;
                        }
                    }
                    //show to open and mClosed PR
                    tvOpened.setText(mOpened + " open ");
                    tvClosed.setText(mClosed + " closed");

                    recyclerView.setAdapter(repositoryAdapter);//adapter set to recyclerview
                    loadingIndicator.hide();//stop the animation load

                    //click item and open the CustomBrowser and view the pullrequest
                    sendPullRequestModelObjectToCustonBroser(mPullRequestArrayList);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(requestJsonObject);
    }

    /**
     * Send the Object Pullrequest to BrowserCuston Activiy
     *
     * @param lista list of repositories
     */
    private void sendPullRequestModelObjectToCustonBroser(final ArrayList<PullRequest> lista) {
        repositoryAdapter.setRecyclerViewOnClickListenerHack(new RecyclerViewOnClickListenerHack() {
            @Override
            public void onClickListener(View view, int position) {
                PullRequest pullRequest = lista.get(position); //get a PullRequest object
                Intent intent = new Intent(PullRequestsActivity.this, BrowserActivity.class);
                intent.putExtra(GitConstants.PULL_REQUEST, pullRequest); //send a parceable pullrequest model
                startActivity(intent);
            }
        });
    }
}
