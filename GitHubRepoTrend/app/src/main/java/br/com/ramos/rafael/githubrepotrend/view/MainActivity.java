package br.com.ramos.rafael.githubrepotrend.view;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paginate.Paginate;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONObject;

import java.util.ArrayList;

import br.com.ramos.rafael.githubrepotrend.R;
import br.com.ramos.rafael.githubrepotrend.adapter.RecyclerViewOnClickListenerHack;
import br.com.ramos.rafael.githubrepotrend.adapter.RepositoryAdapter;
import br.com.ramos.rafael.githubrepotrend.model.Repository;
import br.com.ramos.rafael.githubrepotrend.model.RepositoriesResults;
import br.com.ramos.rafael.githubrepotrend.request.CustomRequestJsonObject;
import br.com.ramos.rafael.githubrepotrend.request.NetworkStateReceiver;
import br.com.ramos.rafael.githubrepotrend.util_.GitConstants;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class MainActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener, View.OnClickListener {

    private static final String REPOSITORY_LIST_SAVE_STATE = "repository_list_save_state"; //key bundle

    private AVLoadingIndicatorView loadingIndicator;
    private ImageView imgLogo;
    private Button btTryConnect;

    private ArrayList<Repository> mRepositoryList;
    private RecyclerView recyclerView;
    private RepositoryAdapter repositoryAdapter;
    private LinearLayoutManager layoutManager;

    private RequestQueue requestQueue;

    private NetworkStateReceiver networkStateReceiver;
    private boolean isConnected = true;

    private Bundle mBundle;

    private static int TOTAL_PAGES = 100;
    private boolean loadingInProgress = false;
    int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBundle = savedInstanceState;

        networkStateReceiver = new NetworkStateReceiver(); //broadcast check network
        networkStateReceiver.addListener(this);

        requestQueue = Volley.newRequestQueue(this); //init volley request

        initWidgetsViews();

        mRepositoryList = new ArrayList<>();

        //ArrayList<Repository> repositoryList = new ArrayList<Repository>();
        //repositoryAdapter = new RepositoryAdapter(this, mRepositoryList);

        loadingInProgress = false;
        page = 0;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(REPOSITORY_LIST_SAVE_STATE, mRepositoryList); //save the current repository list on bundle
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mRepositoryList = savedInstanceState.getParcelableArrayList(REPOSITORY_LIST_SAVE_STATE); //restore the last repository list from bundle
    }

    /**
     * Widgets xml layout referece with java
     */
    private void initWidgetsViews() {
        imgLogo = (ImageView) findViewById(R.id.imgRepoLogo);
        btTryConnect = (Button) findViewById(R.id.btRepoConnect);
        btTryConnect.setOnClickListener(this);
        loadingIndicator = (AVLoadingIndicatorView) findViewById(R.id.loadingRepository);
        recyclerView = (RecyclerView) findViewById(R.id.recylerRepoList);
        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }


    /**
     * Callbacks - Paginate library
     */
    Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            //Load next page/items
            getRepositoriesFromGitHub();
            loadingInProgress = true; //is loading
        }

        @Override
        public boolean isLoading() {
            //A new page is loagins
            return loadingInProgress;
        }

        @Override
        public boolean hasLoadedAllItems() {
            //return true if all items/pages are load
            return page == TOTAL_PAGES;
        }
    };


    /**
     * This method connect to GitHub API using Volley
     */
    private void getRepositoriesFromGitHub() {

        String url = GitConstants.URL_BASE + "search/repositories?q=language:Java&sort=stars&page=" + (page); //List repositories url - pagination number increment to scroll

        //CustonRequestJsonObject response
        CustomRequestJsonObject requestJsonObject = new CustomRequestJsonObject(url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                loadingInProgress = false;
                page++; //increment count page

                if (response != null) { //response JSON

                    Gson gson = new GsonBuilder().create();

                    RepositoriesResults repository = gson.fromJson(response.toString(), RepositoriesResults.class); //Create a parser of Json object Response

                    mRepositoryList.addAll(repository.getItems()); //Add a global list the repositories results

                    repositoryAdapter = new RepositoryAdapter(MainActivity.this, mRepositoryList); //Create the adapter
                    recyclerView.setAdapter(repositoryAdapter); //set adaper

                    //Paginate library configuration
                    Paginate.with(recyclerView, callbacks)
                            .setLoadingTriggerThreshold(4)
                            .build();

                    //Animation load stoped
                    loadingIndicator.hide();

                    //Send the Repository to PullRequestActivity to View detail
                    sendObjectRepositoryModelToPullRequestActivity(mRepositoryList);


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                croutMessage("Error to load data. Try again ", Style.ALERT);//message connection error
            }
        }) {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                //croutMessage("status code" + response.statusCode, Style.ALERT);
                return super.parseNetworkResponse(response);
            }
        };

        requestQueue.add(requestJsonObject); //add jsonrequest to requestQueue
    }

    /**
     * Send the Object Repository to PullRequest Activiy
     *
     * @param listRepository list of repositories
     */
    private void sendObjectRepositoryModelToPullRequestActivity(final ArrayList<Repository> listRepository) {
        repositoryAdapter.setRecyclerViewOnClickListenerHack(new RecyclerViewOnClickListenerHack() {
            @Override
            public void onClickListener(View view, int position) {
                Intent intent = new Intent(MainActivity.this, PullRequestsActivity.class);
                intent.putExtra(GitConstants.REPOSITORY, listRepository.get(position)); //send Model
                startActivity(intent);
            }
        });
    }

    //If Network is available, load data from GitHub
    @Override
    public void networkAvailable() {

        //When the bundle is recreated. The app was changed screen
        //SCREEN - Quando o usuario gira o celular dando a mudanca de tela
        if (mBundle != null) {
            //Animation load stoped
            loadingIndicator.hide();

            ArrayList<Repository> lista = mBundle.getParcelableArrayList(REPOSITORY_LIST_SAVE_STATE); //get the list from Bundle saved

            repositoryAdapter = new RepositoryAdapter(this, lista); //insert to adapter again
            recyclerView.setAdapter(repositoryAdapter); //set adapter

            //Paginate library configuration
            Paginate.with(recyclerView, callbacks)
                    .setLoadingTriggerThreshold(4)
                    .build();

            sendObjectRepositoryModelToPullRequestActivity(lista); //onclicl listener

        } else {
            //The first time when is opended. the bundle is null
            loadingIndicator.show();
            getRepositoriesFromGitHub(); //load data from Github Api
        }

        isConnected = true; //connect flag
        btTryConnect.setVisibility(View.INVISIBLE); //button connect again - offline
        imgLogo.setVisibility(View.INVISIBLE); //logo connect again -offline
    }

    //if network is unavailale, show messages
    @Override
    public void networkUnavailable() {
        isConnected = false; //flag connected
        loadingIndicator.hide(); //animation load false
        croutMessage("Failed to load. Please ensure you're connected to the Internet and try again.", Style.ALERT);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)); //NetWork state connection broadcast
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(networkStateReceiver); // networkStateReceiver broadcast unregister
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Crouton.cancelAllCroutons(); //messages toast crouton lib
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btRepoConnect:
                tryToConnect(); //If network is offilne, show a button for user to try load data again
                break;
        }
    }

    /**
     * Show a message using Crount toast lib
     *
     * @param message text
     * @param style   ALERT-CONFIRM-INFO   Style
     */
    private void croutMessage(String message, Style style) {
        Crouton.makeText(this, message, style).show();
    }

    /**
     * Refresh data manually
     */
    private void tryToConnect() {
        if (isConnected) {
            if (repositoryAdapter != null)
                repositoryAdapter.clearData(); //clear all data from listview(recycler)

            getRepositoriesFromGitHub();
        } else {
            croutMessage("Failed to load. Please ensure you're connected to the Internet and try again.", Style.ALERT);
        }
    }
}
