package br.com.ramos.rafael.githubrepotrend.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import br.com.ramos.rafael.githubrepotrend.R;
import br.com.ramos.rafael.githubrepotrend.model.Repository;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.MyViewHolder> {

    private Context context;
    private LayoutInflater mLayoutInflater;
    private ArrayList<Repository> repositoryList;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;

    public RepositoryAdapter(Context c, ArrayList<Repository> repositoryList) {
        this.mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.repositoryList = repositoryList;
        this.context = c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.iten_repository, viewGroup, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, int position) {

        Repository repository = repositoryList.get(position);

        myViewHolder.tvRepoName.setText(repository.getName());
        myViewHolder.tvRepoDescription.setText(repository.getDescription());

        myViewHolder.tvRepoStarts.setText(repository.getStargazers_count());
        myViewHolder.tvRepoForks.setText(repository.getForks_count());

        myViewHolder.tvAuthorNameFull.setText(repository.getFull_name());

        if (repository.getOwner() != null) {
            myViewHolder.tvAuthorName.setText(repository.getOwner().getLogin());
            Glide.with(context).load(repository.getOwner().getAvatar_url()).placeholder(R.mipmap.ic_launcher).into(myViewHolder.imgAuthor);
        }

    }


    @Override
    public int getItemCount() {
        return this.repositoryList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r) {
        mRecyclerViewOnClickListenerHack = r;
    }

    public void addListItem(Repository horario, int position) {
        this.repositoryList.add(horario);
        notifyItemInserted(position);
    }


    public void removeListItem(int position) {
        this.repositoryList.remove(position);
        notifyItemRemoved(position);
    }

    public void clearData() {
        this.repositoryList.clear();
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvRepoName;
        public TextView tvRepoDescription;
        public TextView tvRepoStarts;
        public TextView tvRepoForks;
        public TextView tvAuthorName;
        public TextView tvAuthorNameFull;
        public ImageView imgAuthor;


        public MyViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            tvRepoName = (TextView) itemView.findViewById(R.id.tvItemRepoName);
            tvRepoDescription = (TextView) itemView.findViewById(R.id.tvItemRepoDescri);
            tvRepoStarts = (TextView) itemView.findViewById(R.id.tvItemRepoStarNumber);
            tvRepoForks = (TextView) itemView.findViewById(R.id.tvItemRepoForkNumber);

            tvAuthorName = (TextView) itemView.findViewById(R.id.tvItemRepoAuthorNick);
            tvAuthorNameFull = (TextView) itemView.findViewById(R.id.tvItemRepoNameFull);
            imgAuthor = (ImageView) itemView.findViewById(R.id.imgItemRepoAuthorPicture);

        }

        @Override
        public void onClick(View v) {
            if (mRecyclerViewOnClickListenerHack != null) {
                mRecyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }


}
