package br.com.ramos.rafael.githubrepotrend.adapter;

import android.view.View;

/**
 * Interface para listener com o recyclerview desde o adapter
 */

public interface RecyclerViewOnClickListenerHack {
    void onClickListener(View view, int position);
}
