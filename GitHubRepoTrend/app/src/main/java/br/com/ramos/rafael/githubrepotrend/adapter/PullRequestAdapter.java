package br.com.ramos.rafael.githubrepotrend.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import net.danlew.android.joda.DateUtils;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import br.com.ramos.rafael.githubrepotrend.R;
import br.com.ramos.rafael.githubrepotrend.model.PullRequest;
import br.com.ramos.rafael.githubrepotrend.util_.GitConstants;
import jp.wasabeef.glide.transformations.CropCircleTransformation;


public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.MyViewHolder> {

    private Context context;
    private LayoutInflater mLayoutInflater;
    private ArrayList<PullRequest> pullRequests;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;

    public PullRequestAdapter(Context c, ArrayList<PullRequest> pullRequests) {
        this.mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.pullRequests = pullRequests;
        this.context = c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.iten_pull_request, viewGroup, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, int position) {

        PullRequest pullRequest = pullRequests.get(position);

        if (pullRequest != null) {
            myViewHolder.tvTitle.setText(pullRequest.getTitle());
            myViewHolder.tvBody.setText(pullRequest.getBody());

            DateTime dateTime = new DateTime(pullRequest.getCreated_at());
            String createdAt = DateUtils.formatDateTime(context, dateTime, DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR);

            myViewHolder.tvDate.setText(createdAt);
            myViewHolder.tvState.setText(pullRequest.getState());

            if (GitConstants.OPEN.equals(pullRequest.getState())) {
                myViewHolder.tvState.setBackgroundColor(context.getResources().getColor(R.color.red));
            } else {
                myViewHolder.tvState.setBackgroundColor(context.getResources().getColor(R.color.green));
            }

            if (pullRequest.getUser() != null) {
                myViewHolder.tvAuthorNick.setText(pullRequest.getUser().getLogin());
                myViewHolder.tvUserType.setText("Type: " + pullRequest.getUser().getType());
                Glide.with(context).load(pullRequest.getUser().getAvatar_url()).placeholder(R.drawable.ic_person).bitmapTransform(new CropCircleTransformation(context)).into(myViewHolder.imgAuthor);
            }
        }


    }


    @Override
    public int getItemCount() {
        return this.pullRequests.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r) {
        mRecyclerViewOnClickListenerHack = r;
    }

    public void addListItem(PullRequest horario, int position) {
        this.pullRequests.add(horario);
        notifyItemInserted(position);
    }


    public void removeListItem(int position) {
        this.pullRequests.remove(position);
        notifyItemRemoved(position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTitle;
        public TextView tvUserType;
        public TextView tvBody;
        public TextView tvAuthorNick;
        public TextView tvDate;
        public ImageView imgAuthor;
        public TextView tvState;


        public MyViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            tvTitle = (TextView) itemView.findViewById(R.id.tvItemPRTitle);
            tvUserType = (TextView) itemView.findViewById(R.id.tvItemPRUserType);
            tvBody = (TextView) itemView.findViewById(R.id.tvItemPRBody);
            tvAuthorNick = (TextView) itemView.findViewById(R.id.tvItemPRAuthorNick);
            tvDate = (TextView) itemView.findViewById(R.id.tvItemPRDate);

            imgAuthor = (ImageView) itemView.findViewById(R.id.imgItemPRAuthorPicture);
            tvState = (TextView) itemView.findViewById(R.id.tvItemPRState);

        }

        @Override
        public void onClick(View v) {
            if (mRecyclerViewOnClickListenerHack != null) {
                mRecyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }


}
