package br.com.ramos.rafael.githubrepotrend.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Rafael Felipe on 16/05/2017.
 *
 * * The Parceable is for save the owner list on Bundle, for restore again when is necessery, and send from Intent
 */

public class Owner implements Parcelable {

    private String login;
    private String avatar_url;
    private String type;

    public Owner(Parcel in) {
        login = in.readString();
        avatar_url = in.readString();
        type = in.readString();
    }

    public Owner() {
    }

    public Owner(String login, String avatar_url, String type) {
        this.login = login;
        this.avatar_url = avatar_url;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(login);
        dest.writeString(avatar_url);
        dest.writeString(type);
    }

    public static final Parcelable.Creator<Owner> CREATOR = new Creator<Owner>() {
        @Override
        public Owner createFromParcel(Parcel in) {
            return new Owner(in);
        }

        @Override
        public Owner[] newArray(int size) {
            return new Owner[size];
        }
    };
}
