package br.com.ramos.rafael.githubrepotrend.request;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Listener for change state of the network
 */
public class NetworkStateReceiver extends BroadcastReceiver {

    protected List<NetworkStateReceiverListener> listeners;
    protected Boolean connected;

    private static boolean mobile_network = false;
    private static boolean wifi_network = false;

    public NetworkStateReceiver() {
        listeners = new ArrayList<>();
        connected = null;
    }


    public void onReceive(Context context, Intent intent) {

        if (intent == null || intent.getExtras() == null)
            return;

        ConnectivityManager connect = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);

        try {
            if (connect != null) {
                mobile_network = connect.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
                wifi_network = connect.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();

                if (mobile_network == true || wifi_network == true) {
                    connected = true;
                } else {
                    connected = false;
                }
            } else {
                connected = false;
            }
        } catch (Exception e) {
            connected = false;
            e.printStackTrace();
        }
        notifyStateToAll();

    }

    private void notifyStateToAll() {
        for (NetworkStateReceiverListener listener : listeners)
            notifyState(listener);
    }

    private void notifyState(NetworkStateReceiverListener listener) {
        if (connected == null || listener == null)
            return;

        if (connected == true)
            listener.networkAvailable();
        else
            listener.networkUnavailable();
    }

    public void addListener(NetworkStateReceiverListener l) {
        listeners.add(l);
        notifyState(l);
    }

    public void removeListener(NetworkStateReceiverListener l) {
        listeners.remove(l);
    }

    public interface NetworkStateReceiverListener {
        public void networkAvailable();

        public void networkUnavailable();
    }
}

