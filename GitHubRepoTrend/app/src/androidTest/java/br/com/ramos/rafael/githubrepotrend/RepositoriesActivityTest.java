package br.com.ramos.rafael.githubrepotrend;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.ramos.rafael.githubrepotrend.util_.GitConstants;
import br.com.ramos.rafael.githubrepotrend.view.BrowserActivity;
import br.com.ramos.rafael.githubrepotrend.view.MainActivity;
import br.com.ramos.rafael.githubrepotrend.view.PullRequestsActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtraWithKey;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;

/**
 * Created by Rafael Felipe on 19/05/2017.
 */

@RunWith(AndroidJUnit4.class)
public class RepositoriesActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class, false, true);


    @Test
    public void initialStateActivity_IsLaunched() {
        onView(withId(R.id.recylerRepoList)).check(matches(isDisplayed()));
    }


}

